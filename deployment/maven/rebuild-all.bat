cd ../../
call mvn clean
if ERRORLEVEL 1 goto end

call mvn install
if ERRORLEVEL 1 goto end

cd deployment/maven
call ant -f build_plugins.xml
if ERRORLEVEL 1 goto end

:end
pause