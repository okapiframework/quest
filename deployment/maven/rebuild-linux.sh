#!/bin/bash -e

cd ../../
mvn -q clean

mvn -q install

cd deployment/maven
ant -f build_plugins.xml
