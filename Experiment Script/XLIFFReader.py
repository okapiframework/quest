import xml.etree.ElementTree as ET

filelist = ['dataset_en_de_barbara.xlf', 'dataset_en_de_beate.xlf', 'dataset_en_de_eugenie.xlf', 'dataset_en_de_sebastian.xlf', 'dataset_en_es_ruben.xlf', 'dataset_en_es_sabrina.xlf']
annocount = {}
setcount = {}
setoccurr = {}
settoanno = {}

count = {'Random':{}, 'Okapi':{}}
rlist = []
nrlist = []
rtotal = 0
nrtotal = 0

for fileitem in filelist:
    rtotal = 0
    nrtotal = 0
    annodata = fileitem[0:len(fileitem)-4].split('_')
    annoset = annodata[1] + '_' + annodata[2]
    annoname = annodata[3]
    annocount[annoname] = 0
    if annoset not in setcount.keys():
        settoanno[annoset] = [annoname]
        setcount[annoset] = 0
        setoccurr[annoset] = 1
    else:
        setoccurr[annoset] += 1
        settoanno[annoset].append(annoname)
    
    print('For annotator ' + annoname + ' of set ' + annoset + ':')
    
    tree = ET.parse(fileitem)
    root = tree.getroot()
    
    prefixUrn = '{urn:oasis:names:tc:xliff:document:1.2}'
    w3prefix = '{http://www.w3.org/2005/11/its}'
    xmlprefix = '{http://www.w3.org/XML/1998/namespace}'
    
    for file in root.iter(prefixUrn + 'file'):
       for body in file.iter(prefixUrn + 'body'):
           for transunit in body.iter(prefixUrn + 'trans-unit'):
               resname = transunit.get('resname')
               typeref = None
               if resname==None:
                   typeref = 'Okapi'
               else:
                   typeref = 'Random'
                   
               locref = transunit.get('{http://www.w3.org/2005/11/its}locQualityIssuesRef')
               if locref!=None:
                   if typeref=='Okapi':
                       nrlist.append(locref)
                   else:
                       rlist.append(locref)
                       
               locref = transunit.get('{http://www.w3.org/2005/11/its}provenanceRecordsRef')
               if locref!=None:
                   if typeref=='Okapi':
                       nrlist.append(locref)
                   else:
                       rlist.append(locref)
                       
               target = transunit.find(prefixUrn + 'target')
               if typeref=='Okapi':
                   nrtotal += float(target.get(w3prefix + 'mtConfidence'))
               else:
                   rtotal += float(target.get(w3prefix + 'mtConfidence'))
    
    
    for locqi in root.iter(w3prefix + 'locQualityIssues'):
        annocount[annoname] += 1
        setcount[annoset] += 1
        
        typeref = None
        locid = '#'+locqi.get(xmlprefix + 'id')
        if locid in rlist:
            typeref = 'Random'
        elif locid in nrlist:
            typeref = 'Okapi'
        else:
            print('ERROR')
            
        for issue in locqi.iter(w3prefix + 'locQualityIssue'):
            issuetype = issue.get('locQualityIssueType')
            if issuetype in count[typeref].keys():
                count[typeref][issuetype] += 1
            else:
                count[typeref][issuetype] = 1
    
    errorlabels = set([])
    for reftype in count.keys():
        errorlabels.update(set(count[reftype].keys()))
    
    print('Error counts for [Random / Okapi]:')
    for issuetype in errorlabels:
        rScore = None
        nrScore = None
        if issuetype in count['Random'].keys():
            rScore = count['Random'][issuetype]
        else:
            rScore = 0
        if issuetype in count['Okapi'].keys():
            nrScore = count['Okapi'][issuetype]
        else:
            nrScore = 0
        print('\t' + issuetype + ': [' + str(rScore) + ' / ' + str(nrScore) + ']')
                    
#     for reftype in count.keys():
#         print('Errors for ' + str(reftype) + ':')
#         for issuetype in errorlabels:
#             if issuetype in count[reftype].keys():
#                 print('\t' + str(issuetype) + ': ' + str(count[reftype][issuetype]))
#             else:
#                 print('\t' + str(issuetype) + ': 0')
#         print('')
            
    print('')
    print('Okapi mtConfidence total: ' + str(nrtotal))
    print('Random mtConfidence total: ' + str(rtotal))
    print('')
    print('-----------------------------------------------------------------------------')

print('Overall data:\n')
# for anno in annocount.keys():
#     print('Total errors for annotator ' + anno + ': ' + str(annocount[anno]))
#     
# print('')
for setitem in setcount.keys():
    print('Total errors in set ' + setitem + ': ' + str(setcount[setitem]))
    for anno in settoanno[setitem]:
        print('\tTotal errors for annotator ' + anno  + ': ' + str(annocount[anno]))
    print('')
    
for setitem in setcount.keys():
    print('Average of errors in set ' + setitem + ': ' + str(setcount[setitem]/setoccurr[setitem]))

        