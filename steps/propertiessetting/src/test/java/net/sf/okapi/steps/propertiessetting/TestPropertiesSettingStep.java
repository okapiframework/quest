/*===========================================================================
  Copyright (C) 2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.steps.propertiessetting;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.filters.FilterConfigurationMapper;
import net.sf.okapi.common.pipelinedriver.BatchItemContext;
import net.sf.okapi.common.pipelinedriver.PipelineDriver;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.steps.common.FilterEventsToRawDocumentStep;
import net.sf.okapi.steps.common.RawDocumentToFilterEventsStep;

import org.junit.Test;
import static org.junit.Assert.*;

public class TestPropertiesSettingStep {

	private final LocaleId locES = LocaleId.SPANISH;
	
	@Test
	public void testSimplePipeline ()
		throws URISyntaxException
	{
		URI inputUri = TestPropertiesSetter.class.getResource("input.html.xlf").toURI();
		File configDir = new File(inputUri).getParentFile();
		// Create the pipeline driver
		PipelineDriver driver = new PipelineDriver();
		// Set the driver parameters (only what is absolutely needed)
		FilterConfigurationMapper fcMapper = new FilterConfigurationMapper();
		fcMapper.addConfigurations("net.sf.okapi.filters.xliff.XLIFFFilter");
		fcMapper.setCustomConfigurationsDirectory(configDir.getAbsolutePath());
		fcMapper.addCustomConfiguration("okf_xliff@updateAltTrans");
		driver.setFilterConfigurationMapper(fcMapper);
		// Add the extraction step
		RawDocumentToFilterEventsStep xs = new RawDocumentToFilterEventsStep();
		driver.addStep(xs);
		// Add the Properties setting step
		PropertiesSettingStep ps = new PropertiesSettingStep();
		driver.addStep(ps);
		// Add the filter writer step
		FilterEventsToRawDocumentStep ms = new FilterEventsToRawDocumentStep();
		driver.addStep(ms);

		// Set the TMX input
		Parameters params = (Parameters)ps.getParameters();
        URL url = TestPropertiesSetter.class.getResource("quest-res01.tmx");
		params.setTmxInput(url.getPath());
	
		// Create the input
		BatchItemContext bic = new BatchItemContext();
		String out = inputUri.getPath() + ".out";
		File outFile = new File(out);
		outFile.delete(); // Make sure last output is removed
		bic.add(new RawDocument(inputUri, "UTF-8", LocaleId.ENGLISH, locES, "okf_xliff@updateAltTrans"),
			outFile.toURI(), "UTF-8");
		driver.addBatchItem(bic);
		
		// Execute the pipeline
		driver.processBatch();
		
		assertTrue(outFile.exists());
		
	}

}
