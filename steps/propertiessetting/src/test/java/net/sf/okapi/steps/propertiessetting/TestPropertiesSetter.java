/*===========================================================================
  Copyright (C) 2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.steps.propertiessetting;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.net.URL;

import net.sf.okapi.common.IResource;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.annotation.GenericAnnotation;
import net.sf.okapi.common.annotation.GenericAnnotationType;
import net.sf.okapi.common.annotation.GenericAnnotations;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.Segment;
import net.sf.okapi.common.resource.TextFragment;
import net.sf.okapi.common.resource.TextUnit;

import org.junit.Test;

public class TestPropertiesSetter {

	private final LocaleId locES = LocaleId.SPANISH;
	
	@Test
	public void testSimpleNotSegmented () {
		PropertiesSetter ps = new PropertiesSetter();
		Parameters params = new Parameters();
        URL url = TestPropertiesSetter.class.getResource("quest-res01.tmx");
		params.setTmxInput(url.getPath());
		ps.initialize(LocaleId.ENGLISH, locES, params);
		
		ITextUnit tu = new TextUnit("id");
		TextFragment trg = tu.createTarget(locES, true, IResource.COPY_ALL).getFirstContent();
		TextFragment src = tu.getSource().getFirstContent();
		// Test on last entry
		src.append("The hotel I am staying in is one of many venues hosting a rakfisk dinner where guests vote on the best, or perhaps the most nasally challenging, fish.");
		trg.append("Me hospedo en el hotel es uno de muchos lugares de organizar una cena rakfisk donde voto invitados en el mejor, o tal vez el más nasal desafiantes, los peces.");
		assertFalse(tu.getSource().hasBeenSegmented());
		ps.process(tu);
		GenericAnnotation ga = tu.getTarget(locES).getAnnotation(GenericAnnotations.class)
			.getFirstAnnotation(GenericAnnotationType.MTCONFIDENCE);
		assertNotNull(ga);
		assertEquals(0.1325079842181034, ga.getDouble(GenericAnnotationType.MTCONFIDENCE_VALUE), 0.0);
		assertEquals("Okapi-QuEst", ga.getString(GenericAnnotationType.ANNOTATORREF));
	}

	@Test
	public void testSimpleSegmented () {
		PropertiesSetter ps = new PropertiesSetter();
		Parameters params = new Parameters();
        URL url = TestPropertiesSetter.class.getResource("quest-res01.tmx");
		params.setTmxInput(url.getPath());
		ps.initialize(LocaleId.ENGLISH, locES, params);
		
		ITextUnit tu = new TextUnit("id");
		Segment src = new Segment("1", new TextFragment("The hotel I am staying in is one of many venues hosting a rakfisk dinner where guests vote on the best, or perhaps the most nasally challenging, fish."));
		tu.getSource().append(src);
		TextFragment trg = tu.createTarget(locES, true, IResource.COPY_SEGMENTATION).getFirstContent();
		trg.append("Me hospedo en el hotel es uno de muchos lugares de organizar una cena rakfisk donde voto invitados en el mejor, o tal vez el más nasal desafiantes, los peces.");
		assertTrue(tu.getSource().hasBeenSegmented());
		ps.process(tu);
		Segment trgSeg = tu.getTarget(locES).getFirstSegment();
		GenericAnnotation ga = trgSeg.getAnnotation(GenericAnnotations.class)
			.getFirstAnnotation(GenericAnnotationType.MTCONFIDENCE);
		assertNotNull(ga);
		assertEquals(0.1325079842181034, ga.getDouble(GenericAnnotationType.MTCONFIDENCE_VALUE), 0.0);
		assertEquals("Okapi-QuEst", ga.getString(GenericAnnotationType.ANNOTATORREF));
	}

}
