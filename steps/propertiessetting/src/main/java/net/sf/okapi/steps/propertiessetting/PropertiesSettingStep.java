/*===========================================================================
  Copyright (C) 2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.steps.propertiessetting;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.IParameters;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.UsingParameters;
import net.sf.okapi.common.pipeline.BasePipelineStep;
import net.sf.okapi.common.pipeline.annotations.StepParameterMapping;
import net.sf.okapi.common.pipeline.annotations.StepParameterType;
import net.sf.okapi.common.resource.ITextUnit;

@UsingParameters(Parameters.class)
public class PropertiesSettingStep extends BasePipelineStep {

//	private static Logger logger = LoggerFactory.getLogger(PropertiesSettingStep.class.getName());

	private Parameters params;
	private LocaleId sourceLocale;
	private LocaleId targetLocale;
	private PropertiesSetter setter;

	public PropertiesSettingStep () {
		params = new Parameters();
	}

	@StepParameterMapping(parameterType = StepParameterType.SOURCE_LOCALE)
	public void setSourceLocale(LocaleId sourceLocale) {
		this.sourceLocale = sourceLocale;
	}

	@SuppressWarnings("deprecation")
	@StepParameterMapping(parameterType = StepParameterType.TARGET_LOCALE)
	public void setTargetLocale(LocaleId targetLocale) {
		this.targetLocale = targetLocale;
	}

	@Override
	public String getName() {
		return "Properties Setting";
	}

	@Override
	public String getDescription() {
		return "Applies specific properties from a TMX document to the input text units. "
			+ "Expects filter events. Returns filter events.";
	}

	@Override
	public IParameters getParameters() {
		return params;
	}

	@Override
	protected Event handleStartBatch(Event event) {
		setter = new PropertiesSetter();
		setter.initialize(sourceLocale, targetLocale, params);
		return event;
	}

	@Override
	protected Event handleTextUnit(Event event) {
		ITextUnit tu = event.getTextUnit();
		setter.process(tu);
		return event;
	}

}
