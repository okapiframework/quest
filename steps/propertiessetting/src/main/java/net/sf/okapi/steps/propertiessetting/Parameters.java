/*===========================================================================
  Copyright (C) 2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.steps.propertiessetting;

import net.sf.okapi.common.EditorFor;
import net.sf.okapi.common.ParametersDescription;
import net.sf.okapi.common.StringParameters;
import net.sf.okapi.common.uidescription.EditorDescription;
import net.sf.okapi.common.uidescription.IEditorDescriptionProvider;
import net.sf.okapi.common.uidescription.PathInputPart;

@EditorFor(Parameters.class)
public class Parameters extends StringParameters implements IEditorDescriptionProvider {

	private static final String TMX_INPUT = "tmxInput";
	private static final String MTCONFIDENCE_FIELD = "mtConfidenceField";

	public Parameters() {
		super();
	}
	
	@Override
	public void reset() {
		super.reset();
		setMtConfidenceField("quest:score");
	}

	public String getTmxInput () {
		return getString(TMX_INPUT);
	}

	public void setTmxInput (String tmxInput){
		setString(TMX_INPUT, tmxInput);
	}
	
	public String getMtConfidenceField () {
		return getString(MTCONFIDENCE_FIELD);
	}

	public void setMtConfidenceField (String mtConfidenceField){
		setString(MTCONFIDENCE_FIELD, mtConfidenceField);
	}
	
	@Override
	public ParametersDescription getParametersDescription() {
		ParametersDescription desc = new ParametersDescription(this);
		desc.add(TMX_INPUT, "TMX input file", "TMX file where the properties to apply are stored");
		desc.add(MTCONFIDENCE_FIELD, "Property type", "Type of the TMX property where the MT Confidence value is stored");
		return desc;
	}

	@Override
	public EditorDescription createEditorDescription(ParametersDescription paramsDesc) {
		EditorDescription desc = new EditorDescription("Properties Setting Step", true, false);

		PathInputPart pip = desc.addPathInputPart(paramsDesc.get(TMX_INPUT), "TMX Input File", false);
		pip.setBrowseFilters("TMX Documents (*.tmx)\tAll Files (*.*)", "*.tmx\t*.*");
		
		desc.addTextInputPart(paramsDesc.get(MTCONFIDENCE_FIELD));
		
		return desc;
	}
	
}
