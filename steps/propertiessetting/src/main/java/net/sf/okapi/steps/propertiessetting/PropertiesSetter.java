/*===========================================================================
  Copyright (C) 2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.steps.propertiessetting;

import java.io.File;
import java.util.HashMap;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.annotation.AltTranslation;
import net.sf.okapi.common.annotation.AltTranslationsAnnotation;
import net.sf.okapi.common.annotation.GenericAnnotation;
import net.sf.okapi.common.annotation.GenericAnnotationType;
import net.sf.okapi.common.annotation.GenericAnnotations;
import net.sf.okapi.common.resource.ISegments;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.Property;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.common.resource.Segment;
import net.sf.okapi.common.resource.TextContainer;
import net.sf.okapi.filters.tmx.TmxFilter;

public class PropertiesSetter {

	private static final String SIGNATURE = "Okapi-QuEst";
	
	private Parameters params;
	private LocaleId srcLoc;
	private LocaleId trgLoc;
	private HashMap<Integer, Double> entries;
	
	public void initialize (LocaleId srcLoc,
		LocaleId trgLoc,
		Parameters params)
	{
		this.params = params;
		this.srcLoc = srcLoc;
		this.trgLoc = trgLoc;
		loadTMX();
	}
	
	public void process (ITextUnit tu) {
		// Do not process if there is no target
		if ( !tu.hasTarget(trgLoc) ) return;
		// Get the segments
		ISegments srcSegs = tu.getSourceSegments();
		TextContainer trgTc = tu.getTarget(trgLoc);
		ISegments trgSegs = tu.getTargetSegments(trgLoc);
		// Process them
		for ( Segment srcSeg : srcSegs ) {
			String segId = srcSeg.getId();
			Segment trgSeg = trgSegs.get(segId);
			if ( trgSeg == null ) continue;
			processSegment(srcSeg, trgSeg, trgTc);
			processAltTrans(srcSeg, trgSeg);
		}
	}
	
	private void processSegment(Segment srcSeg,
		Segment trgSeg,
		TextContainer trgTc)
	{
		int hash = srcSeg.getContent().getText().hashCode();
		// Do we have a match with a score in the TMX file?
		Double score = entries.get(hash);
		if ( score == null ) return; // Nothing to do

		// Annotation needed
		GenericAnnotations anns;
		if ( trgTc.hasBeenSegmented() ) {
			// If it's segmented we place it on the segment
			anns = trgSeg.getAnnotation(GenericAnnotations.class);
		}
		else {
			// If the container is not segmented we need to place the annotation on the container
			// (this will be called only once since in this case there is only one 'segment')
			anns = trgTc.getAnnotation(GenericAnnotations.class);
		}
		// Or create one
		if ( anns == null ) {
			anns = new GenericAnnotations();
			if ( trgTc.hasBeenSegmented() ) trgSeg.setAnnotation(anns);
			else trgTc.setAnnotation(anns);
		}
		
		// Add the MT Confidence annotation
		GenericAnnotation ga = new GenericAnnotation(GenericAnnotationType.MTCONFIDENCE,
			GenericAnnotationType.MTCONFIDENCE_VALUE, score); 
		ga.setString(GenericAnnotationType.ANNOTATORREF, SIGNATURE);
		anns.add(ga);
	}

	private void processAltTrans (Segment srcSeg,
		Segment trgSeg)
	{
		AltTranslationsAnnotation alts = trgSeg.getAnnotation(AltTranslationsAnnotation.class);
		if ( alts == null ) return;
		
		int segHash = srcSeg.getContent().getText().hashCode();
		
		for ( AltTranslation alt : alts ) {
			// Get the hash for the source text (from the alt-trans or the segment)
			int hash = -1;
			ITextUnit tu = alt.getEntry();
			TextContainer trgCont = tu.getTarget(trgLoc);
			if ( trgCont == null ) continue;
			TextContainer srcCont = tu.getSource();
			if ( srcCont == null ) hash = segHash;
			else hash = srcCont.getFirstContent().getText().hashCode();
			// Do we have a match with a score in the TMX file?
			Double score = entries.get(hash);
			if ( score == null ) continue; // If not: move on

			// Else: annotate the alt-trans element
			GenericAnnotations anns = trgCont.getAnnotation(GenericAnnotations.class);
			// Or create one
			if ( anns == null ) {
				anns = new GenericAnnotations();
				trgCont.setAnnotation(anns);
			}
			// Add the MT Confidence annotation
			GenericAnnotation ga = new GenericAnnotation(GenericAnnotationType.MTCONFIDENCE,
				GenericAnnotationType.MTCONFIDENCE_VALUE, score); 
			ga.setString(GenericAnnotationType.ANNOTATORREF, SIGNATURE);
			anns.add(ga);
		}
	}

	private void loadTMX () {
		TmxFilter filter = new TmxFilter();
		try {
			File file = new File(params.getTmxInput());
			RawDocument rd = new RawDocument(file.toURI(), "UTF-8", srcLoc, trgLoc);
			filter.open(rd);
			entries = new HashMap<>();
			while ( filter.hasNext() ) {
				Event event = filter.next();
				if ( event.isTextUnit() ) {
					ITextUnit tu = event.getTextUnit();
					Property prop = tu.getProperty(params.getMtConfidenceField());
					if ( prop == null ) continue;
					String pt = tu.getSource().getFirstContent().getText();
					entries.put(pt.hashCode(), Double.parseDouble(prop.getValue()));
				}
			}
		}
		finally {
			if ( filter != null ) filter.close();
		}
	}

}
