/*===========================================================================
  Copyright (C) 2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.steps.svmmodelbuilder;

import java.io.File;
import java.net.URISyntaxException;

import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.filters.FilterConfigurationMapper;
import net.sf.okapi.common.pipelinedriver.BatchItemContext;
import net.sf.okapi.common.pipelinedriver.PipelineDriver;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.steps.common.RawDocumentToFilterEventsStep;

public class TestSVMModelBuilder {

	//@Test
	public void testOriginalThreeInputs() throws URISyntaxException {

		PipelineDriver driver = setupDriver();
		// Add the QuEst step and its parameters
		SVMModelBuilderStep mbs = new SVMModelBuilderStep();
		driver.addStep(mbs);
		File file = new File(TestSVMModelBuilder.class.getResource("source.en").toURI());
		setDefaultParameters((Parameters)mbs.getParameters(), file);

		// Create the input
		BatchItemContext bic = new BatchItemContext();

		bic.add(new RawDocument(TestSVMModelBuilder.class.getResource(
				"FR.tok").toURI(), "UTF-8", LocaleId.ENGLISH,
				LocaleId.SPANISH, "okf_plaintext"), null, null);
		bic.add(new RawDocument(TestSVMModelBuilder.class.getResource(
				"EN.tok").toURI(), "UTF-8", LocaleId.SPANISH,
				LocaleId.SPANISH, "okf_plaintext"), null, null);
		bic.add(new RawDocument(TestSVMModelBuilder.class.getResource(
				"fr-en_score").toURI(), "UTF-8", LocaleId.ENGLISH,
				LocaleId.SPANISH, "okf_plaintext"), null, null);

		// Add the item to the batch
		driver.addBatchItem(bic);
		// Execute the pipeline
		driver.processBatch();
	}

//	@Test
	public void testXLIFFDocument() throws URISyntaxException {
		PipelineDriver driver = setupDriver();
		// Add the QuEst step and its parameters
		SVMModelBuilderStep mbs = new SVMModelBuilderStep();
		driver.addStep(mbs);
		File file = new File(TestSVMModelBuilder.class.getResource("source.en").toURI());
		setDefaultParameters((Parameters)mbs.getParameters(), file);

		// Create the input
		BatchItemContext bic = new BatchItemContext();
		bic.add(new RawDocument(TestSVMModelBuilder.class.getResource(
				"test01.html.xlf").toURI(), "UTF-8", LocaleId.ENGLISH,
				LocaleId.SPANISH, "okf_xliff"), null, null);
		bic.add(new RawDocument(TestSVMModelBuilder.class.getResource(
				"scores.es").toURI(), "UTF-8", LocaleId.ENGLISH,
				LocaleId.SPANISH, "okf_plaintext"), null, null);

		// Add the item to the batch
		driver.addBatchItem(bic);
		// Execute the pipeline
		driver.processBatch();
	}

	private PipelineDriver setupDriver() {
		// Create the pipeline driver
		PipelineDriver driver = new PipelineDriver();
		// Set the driver parameters (only what is absolutely needed)
		FilterConfigurationMapper fcMapper = new FilterConfigurationMapper();
		fcMapper.addConfigurations("net.sf.okapi.filters.plaintext.PlainTextFilter");
		fcMapper.addConfigurations("net.sf.okapi.filters.xliff.XLIFFFilter");
		driver.setFilterConfigurationMapper(fcMapper);
		// Add the extraction step
		RawDocumentToFilterEventsStep xs = new RawDocumentToFilterEventsStep();
		driver.addStep(xs);
		return driver;
	}

	private void setDefaultParameters(Parameters params, File file) {
		params.setTemporaryFolder(file.getParent());
		// Default resources are in "data"
		String projectRoot = file.getParentFile().getParentFile()
				.getParentFile().getParentFile().getParentFile()
				.getParentFile().getParentFile().getParentFile()
				.getParentFile().getParent();
		String root = projectRoot + "/data";
		params.setFeaturesFile(root
				+ "/config/features/features_blackbox-79.xml");
		params.setGizaFile(root + "/lang_resources/giza/FR2EN.final");
//		params.setSourceNgram(root
//				+ "/lang_resources/english/ngram-counts.europarl-nc.en.proc");
		params.setSourceNgram(root + "/lang_resources/experiment/FR.ngram");
//		params.setSourceNgram("");
		params.setSourceCorpus(root + "/lang_resources/experiment/CORPUS_FR.tok");
		params.setTargetCorpus(root+"/lang_resources/experiment/CORPUS_EN.tok");
		params.setSourceLm(root + "/lang_resources/experiment/FR.lm");
		params.setTargetLm(root + "/lang_resources/experiment/EN.lm");
//		params.setSourceLm("");
//		params.setTargetLm("");
//		params.setSourceLm(root + "/lang_resources/english/lm.nc.en");
//		params.setTargetLm(root + "/lang_resources/spanish/lm.nc.es");
		// Directory of the SRILM executable (with the ending slash!)
		params.setSrilmPath("C:/cygwin64/srilm/bin/cygwin/");
		params.setModelPath(root + "/lang_resources/prediction/SVM_MODEL_FR2EN_MOSES.txt");
	}

}
