/*===========================================================================
  Copyright (C) 2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.steps.svmmodelbuilder;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import routines.svm_scale;
import net.sf.okapi.common.Event;
import net.sf.okapi.common.EventType;
import net.sf.okapi.common.IParameters;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.UsingParameters;
import net.sf.okapi.common.filters.IFilter;
import net.sf.okapi.common.filters.IFilterConfigurationMapper;
import net.sf.okapi.common.pipeline.BasePipelineStep;
import net.sf.okapi.common.pipeline.annotations.StepParameterMapping;
import net.sf.okapi.common.pipeline.annotations.StepParameterType;
import net.sf.okapi.common.resource.ISegments;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.common.resource.Segment;
import net.sf.okapi.common.resource.TextUnitUtil;
import net.sf.okapi.steps.quest.QuestProcessor;

/**
 * Main class for the SVM model builder step.
 * @author GustavoH
 *
 */
@UsingParameters(Parameters.class)
public class SVMModelBuilderStep extends BasePipelineStep {

	private static Logger logger = LoggerFactory
			.getLogger(SVMModelBuilderStep.class.getName());

	private Parameters params;
	private LocaleId sourceLocale;
	private LocaleId targetLocale;
	private ArrayList<String> sourceSentList;
	private ArrayList<String> targetSentList;
	private ArrayList<String> scoresList;
	private RawDocument targetInput;
	private RawDocument scoresInput;
	private SVMModelBuilderProcessor processor;
	private QuestProcessor questProcessor;
	private IFilter targetfilter = null;
	private IFilter scoresfilter = null;
	private IFilterConfigurationMapper fcMapper;

	public SVMModelBuilderStep() {
		params = new Parameters();
	}

	@StepParameterMapping(parameterType = StepParameterType.FILTER_CONFIGURATION_MAPPER)
	public void setFilterConfigurationMapper(IFilterConfigurationMapper fcMapper) {
		this.fcMapper = fcMapper;
	}

	@StepParameterMapping(parameterType = StepParameterType.SOURCE_LOCALE)
	public void setSourceLocale(LocaleId sourceLocale) {
		this.sourceLocale = sourceLocale;
	}

	@SuppressWarnings("deprecation")
	@StepParameterMapping(parameterType = StepParameterType.TARGET_LOCALE)
	public void setTargetLocale(LocaleId targetLocale) {
		this.targetLocale = targetLocale;
	}

	@StepParameterMapping(parameterType = StepParameterType.SECOND_INPUT_RAWDOC)
	public void setSecondInput(RawDocument secondInput) {
		this.targetInput = secondInput;
	}

	@StepParameterMapping(parameterType = StepParameterType.THIRD_INPUT_RAWDOC)
	public void setThirdInput(RawDocument thirdInput) {
		this.scoresInput = thirdInput;
	}

	@Override
	public String getName() {
		return "QuEst SVM Model Builder";
	}

	@Override
	public String getDescription() {
		return "Generates SVM Models to be used by the QuEst Step. Expects filter events. Returns filter events.";
	}

	@Override
	public IParameters getParameters() {
		return params;
	}

	/**
	 * Creates source and target lists of sentences, creates the list of scores,
	 * and instantiates a QuestProcessor object for cross-validation.
	 */
	@Override
	protected Event handleStartBatch(Event event) {
		sourceSentList = new ArrayList<>();
		targetSentList = new ArrayList<>();
		scoresList = new ArrayList<>();

		// The logic below deals with a possible multilingual file being
		// provided along with a scores file.
		if (this.targetInput == null || this.scoresInput == null) {
			if (scoresInput == null && targetInput != null) {
				scoresInput = targetInput;
				targetInput = null;
			} else if (this.targetInput == null && this.scoresInput == null) {
				return event;
			}
		}

		questProcessor = new QuestProcessor(params.getTemporaryFolder(),
				params.getTemporaryFolder() + File.separator + "out", params.getFeaturesFile(),
				params.getLowercaseInput(), params.getGizaFile(),
				params.getSourceNgram(), params.getSourceCorpus(),
				params.getTargetCorpus(), params.getSourceLm(),
				params.getTargetLm(), params.getSrilmPath(),
				null, sourceLocale, targetLocale, false);

		return event;
	}

	@Override
	protected Event handleStartDocument(Event event) {
		// The logic below deals with a possible multilingual file being
		// provided along with a scores file.
		if (this.targetInput == null || this.scoresInput == null) {
			if (scoresInput == null && targetInput != null) {
				scoresInput = targetInput;
				targetInput = null;
			} else if (this.targetInput == null && this.scoresInput == null) {
				return event;
			}
		}

		if (targetInput != null) {
			initializeTargetFilter();
		}
		if (scoresInput != null) {
			initializeScoresFilter();
		}
		return event;
	}

	private void initializeTargetFilter() {
		targetfilter = fcMapper.createFilter(targetInput.getFilterConfigId(),
				null);
		targetfilter.open(targetInput);
	}

	private void initializeScoresFilter() {
		scoresfilter = fcMapper.createFilter(scoresInput.getFilterConfigId(),
				null);
		scoresfilter.open(scoresInput);
	}

	@Override
	protected Event handleEndBatch(Event event) {
		//Create temporary files with all source and target sentences read.
		String tempSource = params.getTemporaryFolder() + File.separator
				+ "TEMPORARY_INPUT_SOURCE.txt";
		String tempTarget = params.getTemporaryFolder() + File.separator
				+ "TEMPORARY_INPUT_TARGET.txt";
		File sourceFile;
		File targetFile;
		BufferedWriter bw = null;
		BufferedWriter outWriter = null;
		BufferedReader outReader = null;
		try {
			sourceFile = new File(tempSource);
			bw = new BufferedWriter(new FileWriter(sourceFile));
			for (String sentence : sourceSentList) {
				bw.write(sentence.trim());
				bw.newLine();
			}
			bw.close();

			targetFile = new File(tempTarget);
			bw = new BufferedWriter(new FileWriter(targetFile));
			for (String sentence : targetSentList) {
				bw.write(sentence.trim());
				bw.newLine();
			}
			bw.close();

			//Run the QuestProcessor object onto source and target sentence files.
			ArrayList<String> result = questProcessor.run(tempSource,
					tempTarget);

			//Delete temporary files.
			sourceFile.delete();
			targetFile.delete();

			File outputFile = new File(params.getModelPath());
			bw = new BufferedWriter(new FileWriter(outputFile.getParentFile()
					+ "/temp_training.txt"));

			//Calculate mininum and maximum of translation score values.
			double lowBoundary = Double.MAX_VALUE;
			double highBoundary = Double.MIN_VALUE;
			for (String s : result) {
				String features = this.formatFeatureValues(s).trim();
				String score = this.scoresList.get(result.indexOf(s));
				double score_d = Double.parseDouble(score);
				if(score_d>highBoundary){
					highBoundary = score_d;
				}
				if(score_d<lowBoundary){
					lowBoundary = score_d;
				}
				if (score != null) {
					bw.write(score + " " + features);
					bw.newLine();
				}
			}
			bw.close();

			//Scale to 0.0~1.0 the feature values calculated by QuestProcessor.
			svm_scale scaler = new svm_scale();
			String[] params_svm = new String[7];
			params_svm[0] = "-o";
			params_svm[1] = outputFile.getParentFile()
					+ "/temp_training_normalized.txt";
			params_svm[2] = "-l";
			params_svm[3] = "-1";
			params_svm[4] = "-u";
			params_svm[5] = "1";
			params_svm[6] = outputFile.getParentFile() + "/temp_training.txt";
			scaler.run(params_svm);

			//Build SVM model files.
			processor = new SVMModelBuilderProcessor();
			processor
					.buildModelFile(
							outputFile.getParentFile()
									+ "/temp_training_normalized.txt",
							outputFile.getParentFile()
									+ "/temp_training_normalized_no_score_range.txt",
							5);

			//Open the model file without the score range.
			outReader = new BufferedReader(new FileReader(
					new File(outputFile.getParentFile()
							+ "/temp_training_normalized_no_score_range.txt")));
			
			//Open the final prediction model file.
			outWriter = new BufferedWriter(new FileWriter(
					new File(params.getModelPath())));
			
			//Write the score range onto the final prediction model file.
			outWriter.write("#Score range: [" + lowBoundary + ", "
					+ highBoundary + "]");
			outWriter.newLine();
			
			//Write the rest of the prediction model onto the final file.
			while(outReader.ready()){
				String line = outReader.readLine().trim();
				outWriter.write(line);
				outWriter.newLine();
			}
			
			//Close the buffered streams.
			outReader.close();
			outWriter.close();
			
			//Force closing of buffered stream.s
			System.gc();
			
			//Delete all temporary files.
			File aux = new File(outputFile.getParentFile()
					+ "/temp_training.txt");
			aux.delete();

			File normalized = new File(outputFile.getParentFile()
					+ "/temp_training_normalized.txt");
			normalized.delete();
			
			File normalizedNoScoreRange = new File(outputFile.getParentFile()
					+ "/temp_training_normalized_no_score_range.txt");
			normalizedNoScoreRange.delete();
			
			File deletionFolder = new File(params.getTemporaryFolder() + File.separator + this.sourceLocale.getLanguage());
			for(File child: deletionFolder.listFiles()){
				child.delete();
			}
			deletionFolder.delete();
			
			deletionFolder = new File(params.getTemporaryFolder() + File.separator + this.targetLocale.getLanguage());
			for(File child: deletionFolder.listFiles()){
				child.delete();
			}
			deletionFolder.delete();
			
			deletionFolder = new File(params.getTemporaryFolder() + File.separator + "out");
			for(File child: deletionFolder.listFiles()){
				child.delete();
			}
			deletionFolder.delete();

		} catch (IOException e) {
			logger.debug("Failed to create output file for QuEst Step");
			logger.debug(e.getLocalizedMessage());
		} finally {
			if (bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					logger.debug(e.getLocalizedMessage());
				}
			}
			if (outWriter != null) {
				try {
					outWriter.close();
				} catch (IOException e) {
					logger.debug(e.getLocalizedMessage());
				}
			}
			if (outReader != null) {
				try {
					outReader.close();
				} catch (IOException e) {
					logger.debug(e.getLocalizedMessage());
				}
			}
		}

		return event;
	}

	@Override
	protected Event handleEndDocument(Event event) {
		return event;
	}

	@Override
	protected Event handleTextUnit(Event sourceEvent) {
		// Process from two input document
		ITextUnit sourceTu = sourceEvent.getTextUnit();
		if (targetInput != null) {
			ITextUnit targetTu = null;

			if (!sourceTu.isTranslatable() || sourceTu.isEmpty()) {
				return sourceEvent;
			}

			// Align the two entries
			Event targetEvent = synchronize(EventType.TEXT_UNIT, sourceTu,
					targetfilter);
			targetTu = targetEvent.getTextUnit();

			sourceSentList.add(sourceTu.toString().trim());
			targetSentList.add(targetTu.toString().trim());

			if (scoresInput != null) {
				// Align the scores entries
				Event scoresEvent = synchronize(EventType.TEXT_UNIT, sourceTu,
						scoresfilter);
				ITextUnit scoresTu = scoresEvent.getTextUnit();
				scoresList.add(scoresTu.toString().trim());
			}
		} else { // Or process from pre-aligned data (e.g. a TMX file)
			ITextUnit tu = sourceEvent.getTextUnit();
			ISegments srcSegs = tu.getSourceSegments();
			if (!tu.hasTarget(targetLocale)) {
				return sourceEvent; // Nothing to do
			}
			ISegments trgSegs = tu.getTargetSegments(targetLocale);
			for (Segment srcSeg : srcSegs) {
				Segment trgSeg = trgSegs.get(srcSeg.getId());
				if (trgSeg == null)
					continue; // Not a pair
				if (srcSeg.getContent().hasText()
						&& trgSeg.getContent().hasText()) {
					sourceSentList
							.add(TextUnitUtil.getText(srcSeg.getContent()));
					targetSentList
							.add(TextUnitUtil.getText(trgSeg.getContent()));
				}
				if (scoresInput != null) {
					// Align the scores entries
					Event scoresEvent = synchronize(EventType.TEXT_UNIT,
							sourceTu, scoresfilter);
					ITextUnit scoresTu = scoresEvent.getTextUnit();
					scoresList.add(scoresTu.toString().trim());
				}
			}
		}

		return sourceEvent;
	}

	/**
	 * Adequate format of feature values.
	 * @param values unformatted string of values
	 * @return formatted string of values
	 */
	private String formatFeatureValues(String values) {
		String[] splitValues = values.trim().split("\t");
		String result = "";
		for (int i = 1; i <= splitValues.length; i++) {
			result += i + ":" + splitValues[i - 1] + " ";
		}
		result = result.trim();
		return result;
	}

	private Event synchronize(EventType untilType, ITextUnit sourceTu,
			IFilter filter) {
		boolean found = false;
		Event event = null;
		while (!found && filter.hasNext()) {
			event = filter.next();
			if (event.isTextUnit()) {
				ITextUnit stu = event.getTextUnit();
				// Skip non-translatable and empty just like our primary filter
				if (stu.isEmpty()) {
					continue;
				}
			}
			found = (event.getEventType() == untilType);
		}
		if (!found) {
			throw new RuntimeException(
					"Different number of source or target TextUnits. "
							+ "The source and target documents are not paragraph aligned at:\n"
							+ "Source: " + sourceTu.getName() + " <> "
							+ sourceTu.getSource().toString());
		}
		return event;
	}

}
