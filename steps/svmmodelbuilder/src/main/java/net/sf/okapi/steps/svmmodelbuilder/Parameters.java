/*===========================================================================
  Copyright (C) 2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.steps.svmmodelbuilder;

import net.sf.okapi.common.EditorFor;
import net.sf.okapi.common.ParametersDescription;
import net.sf.okapi.common.StringParameters;
import net.sf.okapi.common.uidescription.EditorDescription;
import net.sf.okapi.common.uidescription.FolderInputPart;
import net.sf.okapi.common.uidescription.IEditorDescriptionProvider;
import net.sf.okapi.common.uidescription.PathInputPart;
import net.sf.okapi.common.uidescription.TextInputPart;

@EditorFor(Parameters.class)
public class Parameters extends StringParameters implements IEditorDescriptionProvider {

	private static final String TEMPORARY_FOLDER = "temporaryFolder";
	private static final String FEATURES_FILE = "featuresFile";
	private static final String LOWERCASE_INPUT = "lowercaseInput";
	private static final String GIZA_FILE = "gizaFile";
	private static final String SOURCE_NGRAM = "sourceNgram";
	private static final String SOURCE_CORPUS = "sourceCorpus";
	private static final String TARGET_CORPUS = "targetCorpus";
	private static final String SOURCE_LM = "sourceLm";
	private static final String TARGET_LM = "targetLm";
	private static final String SRILM_PATH = "srilmPath";
	private static final String MODEL_PATH = "modelPath";
	
	public String getTargetCorpus() {
		return getString(TARGET_CORPUS);
	}

	public void setTargetCorpus(String targetCorpus) {
		setString(TARGET_CORPUS, targetCorpus);
	}

	public String getTemporaryFolder() {
		return getString(TEMPORARY_FOLDER);
	}

	public void setTemporaryFolder(String temporaryFolder) {
		setString(TEMPORARY_FOLDER, temporaryFolder);
	}
	
	
	public String getModelPath() {
		return getString(MODEL_PATH);
	}

	public void setModelPath(String modelPath) {
		setString(MODEL_PATH, modelPath);
	}

	public String getFeaturesFile() {
		return getString(FEATURES_FILE);
	}

	public void setFeaturesFile(String featuresFile) {
		setString(FEATURES_FILE, featuresFile);
	}

	public boolean getLowercaseInput() {
		return getBoolean(LOWERCASE_INPUT);
	}

	public void setLowercaseInput(boolean lowercaseInput) {
		setBoolean(LOWERCASE_INPUT, lowercaseInput);
	}

	public String getGizaFile() {
		return getString(GIZA_FILE);
	}

	public void setGizaFile(String gizaFile) {
		setString(GIZA_FILE, gizaFile);
	}

	public String getSourceNgram() {
		return getString(SOURCE_NGRAM);
	}

	public void setSourceNgram(String sourceNgram) {
		setString(SOURCE_NGRAM, sourceNgram);
	}

	public String getSourceCorpus() {
		return getString(SOURCE_CORPUS);
	}

	public void setSourceCorpus(String sourceCorpus) {
		setString(SOURCE_CORPUS, sourceCorpus);
	}

	public String getSourceLm() {
		return getString(SOURCE_LM);
	}

	public void setSourceLm(String sourceLm) {
		setString(SOURCE_LM, sourceLm);
	}

	public String getTargetLm() {
		return getString(TARGET_LM);
	}

	public void setTargetLm(String targetLm) {
		setString(TARGET_LM, targetLm);
	}
	
	public String getSrilmPath() {
		return getString(SRILM_PATH);
	}

	public void setSrilmPath(String srilmPath) {
		setString(SRILM_PATH, srilmPath);
	}

	@Override
	public ParametersDescription getParametersDescription() {
		ParametersDescription desc = new ParametersDescription(this);
		// desc.add(INPUT_PATH, "Path to SVM training file",
		// "Determines where the SVM training data will be read from.");
		desc.add(TEMPORARY_FOLDER, "Temporary folder",
				"Path to the folder where temporary files produced by the step will be saved.");
		desc.add(FEATURES_FILE, "Features file",
				"Determines which features should be calculated by QuEst Step");
		desc.add(LOWERCASE_INPUT, "Lowercase the input", null);
		desc.add(GIZA_FILE,
				"Alignment probability file",
				"Determines possible source to target language alignments for the calculation of some features.");
		desc.add(SOURCE_NGRAM, "Source n-gram file",
				"Determines the n-gram counts for the source language.");
		desc.add(SOURCE_CORPUS, "Source training corpus",
				"Corpus required for the calculation of some of QuEst's features.");
		desc.add(TARGET_CORPUS, "Target training Corpus",
				"Corpus required for the calculation of some of QuEst's features.");
		desc.add(SOURCE_LM, "Source language model",
				"Determines the n-gram probability distributions for the source language.");
		desc.add(TARGET_LM, "Target language model",
				"Determines the n-gram probability distributions for the target language.");
		desc.add(SRILM_PATH, "SRILM binaries folder",
				"Determines from which folder should the QuEst Step run SRILM.");
		desc.add(MODEL_PATH, "Prediction model output file",
				"Determines where the model produced by SVM will be saved.");
		return desc;
	}

	@Override
	public EditorDescription createEditorDescription(
			ParametersDescription paramsDesc) {
		EditorDescription desc = new EditorDescription("SVM Model Builder", true, true);

		// PathInputPart pip = desc.addPathInputPart(
		// paramsDesc.get(INPUT_PATH), "SVM Training File Browser", false);
		// pip.setBrowseFilters("All Files (*.*)",
		// "*.*");
		
		desc.addCheckboxPart(paramsDesc.get(LOWERCASE_INPUT));
		
		FolderInputPart fip = desc.addFolderInputPart(paramsDesc.get(TEMPORARY_FOLDER), "Folder Browser");
		fip.setLabelNextToInput(true);

		PathInputPart pip = desc.addPathInputPart(paramsDesc.get(FEATURES_FILE), "Features File", false);
		pip.setBrowseFilters("XML Documents (*.xml)\tAll Files (*.*)", "*.xml\t*.*");
		pip.setLabelNextToInput(true);

		pip = desc.addPathInputPart(paramsDesc.get(GIZA_FILE), "Giza File", false);
		//pip.setBrowseFilters("All files (*.*)", "*.*");
		pip.setLabelNextToInput(true);

		pip = desc.addPathInputPart(paramsDesc.get(SOURCE_NGRAM), "Ngram File", false);
		//pip.setBrowseFilters("All files (*.*)", "*.*");
		pip.setLabelNextToInput(true);

		pip = desc.addPathInputPart(paramsDesc.get(SOURCE_CORPUS), "Source Corpus File", false);
		//pip.setBrowseFilters("All files (*.*)", "*.*");
		pip.setLabelNextToInput(true);

		pip = desc.addPathInputPart(paramsDesc.get(TARGET_CORPUS), "Target Corpus File", false);
		//pip.setBrowseFilters("All files (*.*)", "*.*");
		pip.setLabelNextToInput(true);

		pip = desc.addPathInputPart(paramsDesc.get(SOURCE_LM), "Source Language Model File", false);
		//pip.setBrowseFilters("All files (*.*)", "*.*");
		pip.setLabelNextToInput(true);

		pip = desc.addPathInputPart(paramsDesc.get(TARGET_LM), "Target Language Model File", false);
		//pip.setBrowseFilters("All files (*.*)", "*.*");
		pip.setLabelNextToInput(true);

		fip = desc.addFolderInputPart(paramsDesc.get(SRILM_PATH), "SRLIM Root Folder");
		fip.setLabelNextToInput(true);

		pip = desc.addPathInputPart(paramsDesc.get(MODEL_PATH), "SVM Model File", false);
		//pip.setBrowseFilters("All Files (*.*)", "*.*");
		pip.setLabelNextToInput(true);
		
		return desc;
	}

}
