/*===========================================================================
  Copyright (C) 2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.steps.quest;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import libsvm.svm;
import libsvm.svm_model;
import libsvm.svm_node;
import libsvm.svm_problem;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.XMLWriter;
import net.sf.okapi.common.filterwriter.TMXWriter;
import net.sf.okapi.common.resource.TextFragment;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import routines.svm_scale;
import shef.mt.features.util.FeatureManager;
import shef.mt.features.util.Sentence;
import shef.mt.tools.FileModel;
import shef.mt.tools.Giza;
import shef.mt.tools.LanguageModel;
import shef.mt.tools.NGramExec;
import shef.mt.tools.NGramProcessor;
import shef.mt.tools.PPLProcessor;
import shef.mt.tools.ResourceManager;
import shef.mt.util.NGramSorter;
import shef.mt.util.PropertiesManager;

public class QuestProcessor {
	private static Logger logger = LoggerFactory.getLogger(QuestProcessor.class
			.getName());

	private String input;
	private String output;
	private String sourceFile;
	private String targetFile;
	private LocaleId sourceLoc;
	private LocaleId targetLoc;
	private String sourceLang;
	private String targetLang;
	private boolean lowercaseInput;
	private PropertiesManager resourceManager;
	private FeatureManager featureManager;
	private XMLWriter xmlWriter;
	private TMXWriter tmxWriter;
	private Document xmlReader;
	private NodeList featureList;
	private svm_model predictionModel;
	private double minScore;
	private double maxScore;
	private boolean writeScores;

	public QuestProcessor(String inputFolder, String outputFolder,
			String featuresFile, boolean lowercase, String gizaFile,
			String sourceNGram, String sourceCorpus, String targetCorpus,
			String sourceLM, String targetLM, String srilmPath,
			String modelPath, LocaleId srcLoc, LocaleId trgtLoc,
			boolean writeScores) {
		this.writeScores = writeScores;
		sourceLoc = srcLoc;
		sourceLang = sourceLoc.getLanguage();
		targetLoc = trgtLoc;
		targetLang = trgtLoc.getLanguage();

		// Make sure the temporary input folder exists.
		File tmpDir = new File(inputFolder);
		tmpDir.mkdirs();

		// Make sure the path has a trailing separator (apparently it is needed).
		srilmPath = Util.ensureSeparator(srilmPath, false);

		//Read parameters adequatly.
		parseArguments(inputFolder, outputFolder, featuresFile, lowercase,
				gizaFile, sourceNGram, sourceCorpus, targetCorpus, sourceLM,
				targetLM, srilmPath, modelPath);
		input = resourceManager.getString("input");
		xmlWriter = new XMLWriter(resourceManager.getString("output")
				+ File.separator + "Output_XML.xml");
		tmxWriter = new TMXWriter(resourceManager.getString("output")
				+ File.separator + "Output_TMX.tmx");

		//Read features' XML file.
		featureList = null;
		try {
			xmlReader = DocumentBuilderFactory.newInstance()
					.newDocumentBuilder().parse(featuresFile);
			featureList = xmlReader.getElementsByTagName("feature");
		} catch (SAXException | IOException | ParserConfigurationException e) {
			logger.error("Error while reader the feature file '{}'.",
					featuresFile);
			e.printStackTrace();
		}
		lowercaseInput = lowercase;

		// If prediction scores are required to be outputted, load the SVM
		// model.
		if (this.writeScores) {
			BufferedReader br = null;
			try {
				br = new BufferedReader(new FileReader(new File(modelPath)));
				String[] firstLine = br.readLine().split(" ");
				this.minScore = Double.parseDouble(firstLine[2].substring(1, firstLine[2].length()-1));
				System.out.println("Minscore: " + this.minScore);
				this.maxScore = Double.parseDouble(firstLine[3].substring(0, firstLine[3].length()-1));
				System.out.println("Maxscore: " + this.maxScore);
				br.close();
				
				predictionModel = svm.svm_load_model(modelPath);
			} catch (IOException e) {
				logger.error("Error while loading SVM model '{}'.", modelPath);
				predictionModel = null;
				e.printStackTrace();
				
				if(br!=null){
					try {
						br.close();
					} catch (Exception ex) {
						logger.error("Failed to close buffered stream.");
					}
				}
			}
		}
	}

	/**
	 * Adds all parameters values to the featureManager class attribute.
	 * @param inputFolder
	 * @param outputFolder
	 * @param featuresFile
	 * @param lowercase
	 * @param gizaFile
	 * @param sourceNgram
	 * @param sourceCorpus
	 * @param targetCorpus
	 * @param sourceLM
	 * @param targetLM
	 * @param srilmPath
	 * @param modelPath
	 */
	private void parseArguments(String inputFolder, String outputFolder,
			String featuresFile, boolean lowercase, String gizaFile,
			String sourceNgram, String sourceCorpus, String targetCorpus,
			String sourceLM, String targetLM, String srilmPath, String modelPath) {
		featureManager = new FeatureManager(featuresFile);
		featureManager.setFeatureList("all");
		resourceManager = new PropertiesManager();
		resourceManager.setProperty("logger.on", "false");
		resourceManager.setProperty("shef.mt.copyright",
				"(c) University of Sheffield, 2012");
		resourceManager.setProperty("output", outputFolder);
		resourceManager.setProperty("input", inputFolder);
		resourceManager.setProperty(targetLang + ".lm", targetLM);
		resourceManager.setProperty(sourceLang + ".lm", sourceLM);
		resourceManager.setProperty(sourceLang + ".corpus", sourceCorpus);
		resourceManager.setProperty(targetLang + ".corpus", targetCorpus);
		resourceManager.setProperty(sourceLang + ".ngram", sourceNgram);
		resourceManager.setProperty("pair." + sourceLang + targetLang
				+ ".giza.path", gizaFile);
		resourceManager.setProperty("tools.ngram.path", srilmPath);
		resourceManager.setProperty("tools.ngram.output.ext", ".ppl");
		resourceManager.setProperty("ngramsize", "3");
	}

	/**
	 * Returns the integer value of a string.
	 * @param s string to be parsed.
	 * @return equivalent integer value.
	 */
	private static int atoi(String s) {
		return Integer.parseInt(s);
	}

	/**
	 * Returns the double value of a string.
	 * @param s string to be parsed.
	 * @return equivalent double value.
	 */
	private static double atof(String s) {
		double d = Double.valueOf(s).doubleValue();
		if (Double.isNaN(d) || Double.isInfinite(d)) {
			logger.error("NaN or Infinity in input.");
			System.exit(1);
		}
		return (d);
	}

	/**
	 * Builds an svm_problem object based on a given input file.
	 * @param input_file_name file containing the data to be built the svm_problem of.
	 * @return an svm_problem object representing the input data.
	 */
	private svm_problem buildProblem(String input_file_name) {
		BufferedReader fp = null;
		svm_problem prob = new svm_problem();
		try {
			fp = new BufferedReader(new FileReader(input_file_name));
			Vector<Double> vy = new Vector<Double>();
			Vector<svm_node[]> vx = new Vector<svm_node[]>();
			int max_index = 0;

			while (true) {
				String line = fp.readLine();
				if (line == null) {
					break;
				}
				StringTokenizer st = new StringTokenizer(line, " \t\n\r\f:");

				vy.addElement(atof(st.nextToken()));
				int m = st.countTokens() / 2;
				svm_node[] x = new svm_node[m];
				for (int j = 0; j < m; j++) {
					x[j] = new svm_node();
					x[j].index = atoi(st.nextToken());
					x[j].value = atof(st.nextToken());
				}
				if (m > 0) {
					max_index = Math.max(max_index, x[m - 1].index);
				}
				vx.addElement(x);
			}

			prob = new svm_problem();
			prob.l = vy.size();
			prob.x = new svm_node[prob.l][];
			for (int i = 0; i < prob.l; i++) {
				prob.x[i] = vx.elementAt(i);
			}
			prob.y = new double[prob.l];
			for (int i = 0; i < prob.l; i++) {
				prob.y[i] = vy.elementAt(i);
			}
		} catch (IOException e) {
			logger.error("Error with '{}'.", input_file_name);
			e.printStackTrace();
			return null;
		} finally {
			if (fp != null) {
				try {
					fp.close();
				} catch (IOException e) {
					return null;
				}
			}
		}
		return prob;
	}

	public String getSourceFile() {
		return sourceFile;
	}

	public void setSourceFile(String sourceFile) {
		this.sourceFile = sourceFile;
	}

	public String getTargetFile() {
		return targetFile;
	}

	public void setTargetFile(String targetFile) {
		this.targetFile = targetFile;
	}

	/**
	 * Calculates feature values for translations.
	 * @param srcFile file containing source language sentences.
	 * @param trgtFile file containing target language translations.
	 * @return a list of feature values for each translation.
	 */
	public ArrayList<String> run(String srcFile, String trgtFile) {
		setSourceFile(srcFile);
		setTargetFile(trgtFile);

		constructFolders();
		produceMissingResources(
				this.resourceManager.getProperty(sourceLang + ".corpus"),
				this.resourceManager.getProperty(targetLang + ".corpus"));
		preprocessing();

		ArrayList<String> result = runBB();
		return result;
	}

	/**
	 * Produces linguistic resources not provided by the user.
	 * @param src text file containing source language training sentences.
	 * @param trgt text file containing target language training translations.
	 */
	private void produceMissingResources(String src, String trgt) {

		File lang_resources = null;
		String pathSrcCopy = src;
		String pathTrgtCopy = trgt;
		
		if (resourceManager
				.get("pair." + sourceLang + targetLang + ".giza.path")
				.toString().trim().isEmpty()
				|| resourceManager.get(sourceLang + ".lm").toString().trim()
						.isEmpty()
				|| resourceManager.get(sourceLang + ".ngram").toString().trim()
						.isEmpty()
				|| resourceManager.get(targetLang + ".lm").toString().trim()
						.isEmpty()) {
			lang_resources = new File(resourceManager.getProperty("input")
					.replace("\\", "/") + "/" + "lang_resources");
			if (!lang_resources.exists()) {
				lang_resources.mkdirs();
			}

			pathSrcCopy = src;
			pathTrgtCopy = trgt;
		}

		// Algorithm responsible for producing the missing translation
		// probabilities file from GIZA++.
		if (resourceManager
				.get("pair." + sourceLang + targetLang + ".giza.path")
				.toString().trim().isEmpty()) {

			HashMap<String, HashMap<String, Double>> probabilities = this
					.runIBMModel1(src, trgt, 1);

			String outputPath = resourceManager.getProperty("input")
					+ File.separator + "lang_resources" + File.separator
					+ "translation_probabilities_" + this.sourceLang + "_to_"
					+ this.targetLang + ".prob";
			
			BufferedWriter bw = null;
			try {
				bw = new BufferedWriter(new FileWriter(new File(outputPath)));

				for (String e : probabilities.keySet()) {
					HashMap<String, Double> aux = probabilities.get(e);
					for (String f : aux.keySet()) {
						bw.write(e.trim() + " " + f.trim() + " " + aux.get(f));
						bw.newLine();
					}
				}

				bw.close();

				this.resourceManager.put("pair." + sourceLang + targetLang
						+ ".giza.path", outputPath);
			} catch (IOException e) {
				logger.debug(e.getMessage());
			} finally {
				if (bw != null) {
					try {
						bw.close();
					} catch (Exception e) {
						logger.debug("Failed to close buffered stream.");
					}
				}
			}

		}

		// Algorithm responsible for producing a source language LM with SRILM
		if (resourceManager.get(sourceLang + ".lm").toString().trim().isEmpty()) {
			String[] args = new String[] {
					this.resourceManager.getProperty("tools.ngram.path")
							.replace("\\", "/") + "/" + "ngram-count.exe",
					"-order",
					"3",
					"-text",
					pathSrcCopy,
					"-lm",
					resourceManager.getProperty("input").replace("\\", "/")
							+ "/" + "lang_resources" + "/"
							+ "source_corpus_lm.lm" };
			try {
				Process process = Runtime.getRuntime().exec(args);
				process.waitFor();
				resourceManager.setProperty(sourceLang + ".lm",
						resourceManager.getProperty("input") + File.separator
								+ "lang_resources" + File.separator
								+ "source_corpus_lm.lm");

			} catch (IOException e) {
				logger.debug(e.getLocalizedMessage());
				System.out.println("Error running SRILM");
				e.printStackTrace();
			} catch (InterruptedException e) {
				logger.debug(e.getLocalizedMessage());
				System.out
						.println("Error waiting for SRILM to finish its execution.");
				e.printStackTrace();
			}
			
			System.out.println("##############");
			System.out.println(resourceManager.getProperty("input") + File.separator
								+ "lang_resources" + File.separator
								+ "source_corpus_lm.lm");
			System.out.println("##############");
		}

		// Algorithm responsible for producing a source language LM with SRILM
		if (resourceManager.get(targetLang + ".lm").toString().trim().isEmpty()) {
			String[] args = new String[] {
					this.resourceManager.getProperty("tools.ngram.path")
							.replace("\\", "/") + "/" + "ngram-count.exe",
					"-order",
					"3",
					"-text",
					pathTrgtCopy,
					"-lm",
					resourceManager.getProperty("input").replace("\\", "/")
							+ "/" + "lang_resources" + "/"
							+ "target_corpus_lm.lm" };
			try {
				Process process = Runtime.getRuntime().exec(args);
				process.waitFor();
				resourceManager.setProperty(targetLang + ".lm",
						resourceManager.getProperty("input") + File.separator
								+ "lang_resources" + File.separator
								+ "target_corpus_lm.lm");

			} catch (IOException e) {
				logger.debug(e.getLocalizedMessage());
				System.out.println("Error running SRILM");
				e.printStackTrace();
			} catch (InterruptedException e) {
				logger.debug(e.getLocalizedMessage());
				System.out
						.println("Error waiting for SRILM to finish its execution.");
				e.printStackTrace();
			}
		}

		// Generates a missing ngram counts file.
		if (resourceManager.get(sourceLang + ".ngram").toString().trim()
				.isEmpty()) {
			String[] args = new String[] {
					this.resourceManager.getProperty("tools.ngram.path")
							.replace("\\", "/") + "/" + "ngram-count.exe",
					"-order",
					"3",
					"-text",
					pathSrcCopy,
					"-write",
					resourceManager.getProperty("input").replace("\\", "/")
							+ "/" + "lang_resources" + "/"
							+ "source_corpus_ngrams.ngram" };
			try {
				Process process = Runtime.getRuntime().exec(args);
				process.waitFor();

				String cleanNgramPath = resourceManager.getProperty("input")
						.replace("\\", "/")
						+ "/"
						+ "lang_resources"
						+ "/"
						+ "source_corpus_ngrams.ngram";

				NGramSorter.run(cleanNgramPath, 4, 3, 2, cleanNgramPath);

				resourceManager.setProperty(sourceLang + ".ngram",
						resourceManager.getProperty("input") + File.separator
								+ "lang_resources" + File.separator
								+ "source_corpus_ngrams.ngram.clean");

			} catch (IOException e) {
				logger.debug(e.getLocalizedMessage());
				System.out.println("Error running SRILM");
				e.printStackTrace();
			} catch (InterruptedException e) {
				logger.debug(e.getLocalizedMessage());
				System.out
						.println("Error waiting for SRILM to finish its execution.");
				e.printStackTrace();
			}
		}

	}

	/**
	 * Obsolete algorithm that creates an IBM 1 translation model.
	 * @param src_corpus source language corpus.
	 * @param trgt_corpus target language corpus.
	 * @param iterations number of desired iterations.
	 * @return map of type <word_1, word_2> -> probability of translation
	 */
	private HashMap<String, HashMap<String, Double>> runIBMModel1(
			String src_corpus, String trgt_corpus, int iterations) {

		BufferedReader br1 = null;
		BufferedReader br2 = null;

		System.out.println("Initialization initiated...");
		try {
			br1 = new BufferedReader(new FileReader(new File(src_corpus)));
			br2 = new BufferedReader(new FileReader(new File(trgt_corpus)));

			HashMap<String, HashMap<String, Double>> t = new HashMap<String, HashMap<String, Double>>();
			HashMap<String, Integer> fwords = new HashMap<String, Integer>();
			HashMap<String, Integer> ewords = new HashMap<String, Integer>();
			double counter = 0;
			while (br1.ready() && br2.ready()) {
				String src = br1.readLine().trim();
				String trg = br2.readLine().trim();

				String[] src_v = src.split(" ");
				String[] trg_v = trg.split(" ");
				for (String ws : src_v) {
					if (!ewords.containsKey(ws)) {
						ewords.put(ws, 0);
					}
					for (String ts : trg_v) {
						if (!fwords.containsKey(ts)) {
							counter++;
							fwords.put(ts, 0);
						}
						if (!t.containsKey(ws)) {
							HashMap<String, Double> aux = new HashMap<String, Double>();
							aux.put(ts, 0.0);
							t.put(ws, aux);
						} else {
							HashMap<String, Double> aux = t.get(ws);
							aux.put(ts, 0.0);
							t.put(ws, aux);
						}
					}
				}
			}
			double uniform = 1.0 / counter;

			System.out.println("Initialization finished.");
			for (String key : t.keySet()) {
				HashMap<String, Double> aux = t.get(key);
				for (String key_t : aux.keySet()) {
					aux.put(key_t, uniform);
				}
				t.put(key, aux);
			}

			br1.close();
			br2.close();

			System.out.println("Iteration started...");
			for (int i = 0; i < iterations; i++) {

				br1 = new BufferedReader(new FileReader(new File(src_corpus)));
				br2 = new BufferedReader(new FileReader(new File(trgt_corpus)));

				HashMap<String, Double> total = new HashMap<String, Double>();
				HashMap<String, HashMap<String, Double>> count = new HashMap<String, HashMap<String, Double>>();
				for (String fword : fwords.keySet()) {
					total.put(fword, 0.0);
					for (String eword : ewords.keySet()) {
						if (count.containsKey(eword)) {
							HashMap<String, Double> aux = count.get(eword);
							aux.put(fword, 0.0);
							count.put(eword, aux);
						} else {
							HashMap<String, Double> aux = new HashMap<String, Double>();
							aux.put(fword, 0.0);
							count.put(eword, aux);
						}
					}
				}

				while (br1.ready() && br2.ready()) {
					String src = br1.readLine().trim();
					String trg = br2.readLine().trim();
					String[] src_v = src.split(" ");
					String[] trg_v = trg.split(" ");

					HashMap<String, Double> total_s = new HashMap<String, Double>();
					for (String sw : src_v) {
						total_s.put(sw, 0.0);
						for (String tw : trg_v) {
							Double aux = total_s.get(sw);
							total_s.put(sw, aux + t.get(sw).get(tw));
						}
					}
					for (String sw : src_v) {
						for (String tw : trg_v) {
							HashMap<String, Double> aux = count.get(sw);
							Double auxd = aux.get(tw);
							auxd += t.get(sw).get(tw) / total_s.get(sw);
							aux.put(tw, auxd);
							count.put(sw, aux);

							auxd = total.get(tw);
							auxd += t.get(sw).get(tw) / total_s.get(sw);
							total.put(tw, auxd);
						}
					}
				}

				for (String f : fwords.keySet()) {
					for (String e : ewords.keySet()) {
						HashMap<String, Double> aux = t.get(e);
						aux.put(f, count.get(e).get(f) / total.get(f));
						t.put(e, aux);
					}
				}

				br1.close();
				br2.close();
			}

			System.out.println("Iterations finished.");
			return t;

		} catch (FileNotFoundException ex) {
			logger.debug(ex.getLocalizedMessage());
		} catch (IOException ex) {
			logger.debug(ex.getMessage());
		} finally {
			if (br1 != null) {
				try {
					br1.close();
				} catch (Exception e) {
					logger.debug("Failed to close buffered stream.");
				}
			}
			if (br2 != null) {
				try {
					br2.close();
				} catch (Exception e) {
					logger.debug("Failed to close buffered stream.");
				}
			}
		}
		return null;

	}

	/**
	 * Calculates feature values for the translations given.
	 * @return list of feature values for each of the translations.
	 */
	private ArrayList<String> runBB() {
		ArrayList<String> result = new ArrayList<>();

		File f = new File(sourceFile);
		String sourceFileName = f.getName();
		f = new File(targetFile);
		String targetFileName = f.getName();
		String outputFileName = "Output_Raw.txt";
		String out = resourceManager.getString("output") + File.separator
				+ outputFileName;
		logger.debug("Output will be: " + out);

		String pplSourcePath = resourceManager.getString("input")
				+ File.separator + sourceLang + File.separator + sourceFileName
				+ resourceManager.getString("tools.ngram.output.ext");
		String pplTargetPath = resourceManager.getString("input")
				+ File.separator + targetLang + File.separator + targetFileName
				+ resourceManager.getString("tools.ngram.output.ext");

		runNGramPPL();

		PPLProcessor pplProcSource = new PPLProcessor(pplSourcePath,
				new String[] { "logprob", "ppl", "ppl1" });
		PPLProcessor pplProcTarget = new PPLProcessor(pplTargetPath,
				new String[] { "logprob", "ppl", "ppl1" });

		// FileModel fm = new FileModel(sourceFile,
		// resourceManager.getString(sourceLang + ".corpus"));

		// loadGiza();
		processNGrams();

		xmlWriter.writeStartDocument();
		xmlWriter.writeStartElement("root");

		tmxWriter.writeStartDocument(sourceLoc, targetLoc, null, null,
				"sentence", null, "plaintext");

		BufferedReader brSource = null;
		BufferedReader brTarget = null;
		BufferedReader posSource = null;
		BufferedReader posTarget = null;
		BufferedWriter output = null;
		BufferedWriter tempNorm = null;
		try {
			brSource = new BufferedReader(new FileReader(sourceFile));
			brTarget = new BufferedReader(new FileReader(targetFile));
			output = new BufferedWriter(new FileWriter(out));
			posSource = null;
			posTarget = null;

			ResourceManager.printResources();
			Sentence sourceSent;
			Sentence targetSent;
			int sentCount = 0;

			String lineSource = brSource.readLine();
			String lineTarget = brTarget.readLine();

			// Lists to store source and target sentences in sequence
			ArrayList<Sentence> sourceSentences = new ArrayList<>();
			ArrayList<Sentence> targetSentences = new ArrayList<>();

			// File to store temporarily non-normalized feature values
			File nonNorm = new File(resourceManager.getString("output")
					+ File.separator + "temp_non_normalized.txt");
			tempNorm = new BufferedWriter(new FileWriter(nonNorm));

			while ((lineSource != null) && (lineTarget != null)) {

				sourceSent = new Sentence(lineSource, sentCount);
				targetSent = new Sentence(lineTarget, sentCount);

				sourceSent.computeNGrams(3);
				targetSent.computeNGrams(3);
				pplProcSource.processNextSentence(sourceSent);
				pplProcTarget.processNextSentence(targetSent);

				++sentCount;
				String featureValues = featureManager.runFeatures(sourceSent,
						targetSent);
				result.add(featureValues);

				// Save source and target sentences in lists
				sourceSentences.add(sourceSent);
				targetSentences.add(targetSent);

				// Write SVM formatted feature values onto temporary
				// non-normalized features file
				String formattedFeatureValues = formatFeatureValues(featureValues);
				tempNorm.write("0.0 " + formattedFeatureValues);
				tempNorm.newLine();

				output.write(featureValues);
				output.newLine();

				lineSource = brSource.readLine();
				lineTarget = brTarget.readLine();
			}
			tempNorm.close();

			// Normalize the non-normalized temporary file feature values
			svm_scale scaler = new svm_scale();
			String[] params_svm = new String[7];
			params_svm[0] = "-o";
			params_svm[1] = resourceManager.getString("output")
					+ File.separator + "temp_normalized.txt";
			params_svm[2] = "-l";
			params_svm[3] = "-1";
			params_svm[4] = "-u";
			params_svm[5] = "1";
			params_svm[6] = resourceManager.getString("output")
					+ File.separator + "temp_non_normalized.txt";
			scaler.run(params_svm);

			// Read the normalized feature values
			ArrayList<String> normalizedFeatureValues = extractFeatureValuesFromSVMFile(resourceManager
					.getString("output")
					+ File.separator
					+ "temp_normalized.txt");

			// Write the values produced
			for (int i = 0; i < normalizedFeatureValues.size(); i++) {
				String values = normalizedFeatureValues.get(i);
				Sentence source = sourceSentences.get(i);
				Sentence target = targetSentences.get(i);

				writeDataToXMLFile(source, target, values);
				writeDataToTMXFile(source, target, values);
			}

			File deleter = new File(resourceManager.getString("output")
					+ File.separator + "temp_non_normalized.txt");
			deleter.delete();

			deleter = new File(resourceManager.getString("output")
					+ File.separator + "temp_normalized.txt");
			deleter.delete();

			xmlWriter.writeEndElement(); // closes root
			xmlWriter.writeEndDocument();
			xmlWriter.close();

			tmxWriter.writeEndDocument();
			tmxWriter.close();
			brSource.close();
			brTarget.close();
			output.close();
		} catch (Exception e) {
			logger.error(e.getLocalizedMessage());
		} finally {
			if (brSource != null) {
				try {
					brSource.close();
				} catch (Exception e) {
					logger.debug("Failed to close buffered stream.");
				}
			}
			if (brTarget != null) {
				try {
					brTarget.close();
				} catch (Exception e) {
					logger.debug("Failed to close buffered stream.");
				}
			}
			if (posSource != null) {
				try {
					posSource.close();
				} catch (Exception e) {
					logger.debug("Failed to close buffered stream.");
				}
			}
			if (posTarget != null) {
				try {
					posTarget.close();
				} catch (Exception e) {
					logger.debug("Failed to close buffered stream.");
				}
			}
			if (output != null) {
				try {
					output.close();
				} catch (Exception e) {
					logger.debug("Failed to close buffered stream.");
				}
			}
			if (tempNorm != null) {
				try {
					tempNorm.close();
				} catch (Exception e) {
					logger.debug("Failed to close buffered stream.");
				}
			}
		}
		return result;
	}

	/**
	 * Extracts the feature values from a given SVM training file.
	 * @param path text file containing the training data.
	 * @return an array containing the feature values extracted.
	 */
	private ArrayList<String> extractFeatureValuesFromSVMFile(String path) {
		ArrayList<String> result = new ArrayList<>();

		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(new File(path)));
			while (br.ready()) {
				String line = br.readLine().trim();
				if (!line.trim().isEmpty()) {
					String[] data = line.split(" ");

					String rawValues = "";
					for (int i = 1; i < data.length; i++) {
						String[] featureData = data[i].split(":");
						rawValues += featureData[1].trim() + "\t";
					}
					rawValues = rawValues.trim();
					result.add(rawValues);
				}
			}
			br.close();
		} catch (FileNotFoundException e) {
			logger.error(e.getLocalizedMessage());
		} catch (IOException e) {
			logger.error(e.getLocalizedMessage());
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (Exception e) {
					logger.error("Failed to close buffered stream.");
				}
			}
		}
		return result;
	}

	/**
	 * Writes predicted translation quality estimation scores into the output TMX file.
	 * @param sourceSent source language sentence.
	 * @param targetSent target language translation.
	 * @param featureValues feature values estimated for translation.
	 */
	private void writeDataToTMXFile(Sentence sourceSent, Sentence targetSent,
			String featureValues) {
		Map<String, String> atts = new HashMap<>();
		TextFragment srcFrag = new TextFragment(sourceSent.getText());
		TextFragment trgFrag = new TextFragment(targetSent.getText());
		atts.put("quest:features", featureValues);

		if (writeScores) {
			String formattedFeatureValues = formatFeatureValues(featureValues);
			svm_node[] finalFeatureValues = buildSvm_Node(formattedFeatureValues);
			double score = svm.svm_predict(predictionModel, finalFeatureValues);
			atts.put("quest:originalScore", "" + score);
			atts.put("quest:score", "" + rescaleScore(minScore, maxScore, score));
		}

		tmxWriter.writeTU(srcFrag, trgFrag, null, atts);

	}

	/**
	 * Re-scales quality predictions into a 0.0~1.0 interval.
	 * @param low minimum prediction score possible.
	 * @param high maximum prediction score possible.
	 * @param value predicted score.
	 * @return rescaled score between 0.0 and 1.0.
	 */
	public double rescaleScore(double low, double high, double value) {
		double auxLow, auxHigh;
		if (low>high){
			auxLow = high;
			auxHigh = low;
		}else{
			auxLow = low;
			auxHigh = high;
		}
		if (value > auxHigh)
			value = auxHigh; // Safety
		double m = 0.0; // low value of map to range
		double n = 1.0; // high value of map to range
		double result = (m + ((value - auxLow) / (auxHigh - auxLow) * (n - m)));
		if (low>high){
			return n-result;
		}else{
			return result;
		}
	}

	/**
	 * Writes predicted translation quality estimation scores into the output XML file.
	 * @param sourceSent source language sentence.
	 * @param targetSent target language translation.
	 * @param featureValues feature values estimated for translation.
	 */
	private void writeDataToXMLFile(Sentence sourceSent, Sentence targetSent,
			String featureValues) {
		xmlWriter.writeStartElement("translation");
		xmlWriter.writeStartElement("source");
		xmlWriter.writeString(sourceSent.getText());
		xmlWriter.writeEndElement();
		xmlWriter.writeLineBreak();
		xmlWriter.writeStartElement("target");
		xmlWriter.writeString(targetSent.getText());
		xmlWriter.writeEndElement();
		xmlWriter.writeLineBreak();
		xmlWriter.writeStartElement("features");
		xmlWriter.writeString(featureValues);
		xmlWriter.writeEndElement();
		xmlWriter.writeLineBreak();

		// If set to write scores, calculate predicton score and add it to
		// output file.
		if (this.writeScores) {
			String formattedFeatureValues = formatFeatureValues(featureValues);
			svm_node[] finalFeatureValues = buildSvm_Node(formattedFeatureValues);
			double score = svm.svm_predict(predictionModel, finalFeatureValues);
			xmlWriter.writeStartElement("score");
			xmlWriter.writeString(score + "");
			xmlWriter.writeEndElement();
			xmlWriter.writeLineBreak();
		}

		xmlWriter.writeEndElement();
		xmlWriter.writeLineBreak();
		xmlWriter.writeLineBreak();
	}

	/**
	 * Creates an svm_node vector for a given string of feature values.
	 * @param s string containing all feature values of a translation.
	 * @return an svm_node vector equivalent to the feature values of the input string.
	 */
	private static svm_node[] buildSvm_Node(String s) {
		String[] features = s.trim().split(" ");
		int size = features.length;
		if (!features[0].contains(":")) {
			String[] aux = new String[size - 1];
			for (int i = 0; i < size - 1; i++) {
				aux[i] = features[i + 1];
			}
			features = aux;
			size = size - 1;
		}
		svm_node[] result = new svm_node[size];
		for (int i = 0; i < size; i++) {
			String feature = features[i];
			String[] data = feature.split(":");
			String index = data[0];
			String value = data[1];
			svm_node aux = new svm_node();
			aux.index = Integer.valueOf(index);
			aux.value = Double.valueOf(value);
			result[i] = aux;
		}
		return result;
	}

	/**
	 * Adequate format of feature values.
	 * @param values unformatted string of values
	 * @return formatted string of values
	 */
	private String formatFeatureValues(String values) {
		String[] splitValues = values.trim().split("\t");
		String result = "";
		for (int i = 1; i <= splitValues.length; i++) {
			result += i + ":" + splitValues[i - 1] + " ";
		}
		result = result.trim();
		return result;
	}

	/**
	 * Produces a language model object based on an n-gram counts file.
	 * @return language model object.
	 */
	private LanguageModel processNGrams() {
		NGramProcessor ngp = new NGramProcessor(
				resourceManager.getString(sourceLang + ".ngram"));
		return ngp.run();
	}

	/**
	 * Obsolete function that loads a Giza object onto memory.
	 */
	private void loadGiza() {
		String gizaPath = resourceManager.getString("pair." + sourceLang
				+ targetLang + ".giza.path");
		logger.debug("Giza path: {}", gizaPath);
		// TODO: do we need to load this?
		Giza giza = new Giza(gizaPath);
	}

	/**
	 * Calculates perplexity measures for the input data.
	 */
	private void runNGramPPL() {
		NGramExec nge = new NGramExec(
				resourceManager.getString("tools.ngram.path"));
		logger.debug("runNgramPPL");
		File f = new File(sourceFile);
		String sourceOutput = input + File.separator + sourceLang
				+ File.separator + f.getName() + ".ppl";

		f = new File(targetFile);
		String targetOutput = input + File.separator + targetLang
				+ File.separator + f.getName() + ".ppl";

		nge.runNGramPerplex(sourceFile, sourceOutput,
				resourceManager.getString(sourceLang + ".lm"));
		logger.debug(resourceManager.getString(targetLang + ".lm"));
		nge.runNGramPerplex(targetFile, targetOutput,
				resourceManager.getString(targetLang + ".lm"));
		
	}

	/**
	 * Builds folders for output files.
	 */
	public void constructFolders() {
		File f = new File(input);
		if (!f.exists()) {
			f.mkdirs();
		}

		f = new File(input + File.separator + sourceLang);
		if (!f.exists()) {
			f.mkdirs();
		}

		f = new File(input + File.separator + targetLang);
		if (!f.exists()) {
			f.mkdirs();
		}

		f = new File(resourceManager.getString("output"));
		if (!f.exists()) {
			f.mkdirs();
		}
	}

	/**
	 * Preprocesses data before the calculation of feature values.
	 */
	private void preprocessing() {

		// Setup initial file paths.
		String sourceInputFolder = input + File.separator + sourceLang;
		String targetInputFolder = input + File.separator + targetLang;
		File origSourceFile = new File(sourceFile);
		File inputSourceFile = new File(sourceInputFolder + File.separator
				+ origSourceFile.getName());
		logger.debug("source input:" + sourceFile);
		logger.debug("target input:" + targetFile);
		File origTargetFile = new File(targetFile);
		File inputTargetFile = new File(targetInputFolder + File.separator
				+ origTargetFile.getName());

		// Copy input files to output folder.
		try {
			logger.debug("copying input to " + inputSourceFile.getPath());
			copyFile(origSourceFile, inputSourceFile);
			logger.debug("copying input to " + inputTargetFile.getPath());
			copyFile(origTargetFile, inputTargetFile);
		} catch (Exception e) {
			logger.error(e.getLocalizedMessage());
		}

		// If user selected lowercasing option, create intermediate lowercased
		// input file.
		if (this.lowercaseInput) {
			BufferedReader br = null;
			BufferedWriter bw = null;
			try {
				br = new BufferedReader(new FileReader(inputSourceFile));
				bw = new BufferedWriter(new FileWriter(new File(inputSourceFile
						+ ".lowercase")));
				while (br.ready()) {
					String sentence = br.readLine();
					sentence = sentence.toLowerCase();
					bw.write(sentence.trim());
					bw.newLine();
				}
				br.close();
				bw.close();
			} catch (FileNotFoundException e) {
				logger.error("Failed to create a reader to read the inputSourceFile.");
				e.printStackTrace();
			} catch (IOException e) {
				logger.error("Failed to check for ready() state for inputSourceFile.");
				e.printStackTrace();
			} finally {
				if (br != null) {
					try {
						br.close();
					} catch (Exception e) {
						logger.debug("Failed to close buffered stream.");
					}
				}
				if (bw != null) {
					try {
						bw.close();
					} catch (Exception e) {
						logger.debug("Failed to close buffered stream.");
					}
				}
			}

			br = null;
			bw = null;
			try {
				br = new BufferedReader(new FileReader(inputTargetFile));
				bw = new BufferedWriter(new FileWriter(new File(inputTargetFile
						+ ".lowercase")));
				while (br.ready()) {
					String sentence = br.readLine();
					sentence = sentence.toLowerCase();
					bw.write(sentence.trim());
					bw.newLine();
				}
				br.close();
				bw.close();
			} catch (FileNotFoundException e) {
				logger.error("Failed to create a reader to read the inputTargetFile.");
				e.printStackTrace();
			} catch (IOException e) {
				logger.error("Failed to check for ready() state for inputTargetFile.");
				e.printStackTrace();
			} finally {
				if (br != null) {
					try {
						br.close();
					} catch (Exception e) {
						logger.debug("Failed to close buffered stream.");
					}
				}
				if (bw != null) {
					try {
						bw.close();
					} catch (Exception e) {
						logger.debug("Failed to close buffered stream.");
					}
				}
			}

			sourceFile = inputSourceFile + ".lowercase";
			targetFile = inputTargetFile + ".lowercase";
		}
	}

	/**
	 * Creates a copy of a given file.
	 * @param sourceFile file to be copied.
	 * @param destFile path of copied file.
	 * @throws IOException throws exception if file to be copied does not exists.
	 */
	private void copyFile(File sourceFile, File destFile) throws IOException {
		if (sourceFile.equals(destFile)) {
			return;
		}
		if (!destFile.exists()) {
			destFile.createNewFile();
		}

		java.nio.channels.FileChannel source = null;
		java.nio.channels.FileChannel destination = null;
		try {
			source = new FileInputStream(sourceFile).getChannel();
			destination = new FileOutputStream(destFile).getChannel();
			destination.transferFrom(source, 0, source.size());
		} finally {
			if (source != null) {
				source.close();
			}
			if (destination != null) {
				destination.close();
			}
		}
	}

	public PropertiesManager getResourceManager() {
		return resourceManager;
	}

}
