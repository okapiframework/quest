/*===========================================================================
  Copyright (C) 2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.steps.quest;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.EventType;
import net.sf.okapi.common.IParameters;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.UsingParameters;
import net.sf.okapi.common.filters.IFilter;
import net.sf.okapi.common.filters.IFilterConfigurationMapper;
import net.sf.okapi.common.pipeline.BasePipelineStep;
import net.sf.okapi.common.pipeline.annotations.StepParameterMapping;
import net.sf.okapi.common.pipeline.annotations.StepParameterType;
import net.sf.okapi.common.resource.ISegments;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.common.resource.Segment;
import net.sf.okapi.common.resource.TextUnitUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Main class of the Quest step.
 * @author GustavoH
 *
 */
@UsingParameters(Parameters.class)
public class QuestStep extends BasePipelineStep {

	private static Logger logger = LoggerFactory.getLogger(QuestStep.class.getName());

	private Parameters params;
	private LocaleId sourceLocale;
	private LocaleId targetLocale;
	private RawDocument targetInput;
	private IFilter filter = null;
	private IFilterConfigurationMapper fcMapper;
	private ArrayList<String> sourceSentList;
	private ArrayList<String> targetSentList;
	private QuestProcessor questProcessor;

	public QuestStep() {
		params = new Parameters();
	}

	@StepParameterMapping(parameterType = StepParameterType.SOURCE_LOCALE)
	public void setSourceLocale(LocaleId sourceLocale) {
		this.sourceLocale = sourceLocale;
	}

	@StepParameterMapping(parameterType = StepParameterType.FILTER_CONFIGURATION_MAPPER)
	public void setFilterConfigurationMapper(IFilterConfigurationMapper fcMapper) {
		this.fcMapper = fcMapper;
	}

	@SuppressWarnings("deprecation")
	@StepParameterMapping(parameterType = StepParameterType.TARGET_LOCALE)
	public void setTargetLocale(LocaleId targetLocale) {
		this.targetLocale = targetLocale;
	}

	@StepParameterMapping(parameterType = StepParameterType.SECOND_INPUT_RAWDOC)
	public void setSecondInput(RawDocument secondInput) {
		this.targetInput = secondInput;
	}

	@Override
	public String getName() {
		return "QuEst Quality Estimation";
	}

	@Override
	public String getDescription() {
		return "Calculates feature values of translations for quality estimation. Expects filter events. Returns filter events.";
	}

	@Override
	public IParameters getParameters() {
		return params;
	}

	private void initializeTargetFilter() {
		logger.debug("Has target input file: {}", targetInput.getInputURI().toString());
		filter = fcMapper.createFilter(targetInput.getFilterConfigId(), null);
		filter.open(targetInput);
	}

	@Override
	protected Event handleStartBatch(Event event) {
		//Allocate source and target sentence lists
		sourceSentList = new ArrayList<>();
		targetSentList = new ArrayList<>();
		
		//Allocate a QuestProcessor instance with all parameters required.
		questProcessor = new QuestProcessor(params.getInputFolder(),
			params.getOutputFolder(), params.getFeaturesFile(),
			params.getLowercaseInput(), params.getGizaFile(),
			params.getSourceNgram(), params.getSourceCorpus(), params.getTargetCorpus(),
			params.getSourceLm(), params.getTargetLm(),
			params.getSrilmPath(), params.getModelPath(),
			sourceLocale, targetLocale, true);
		
		logger.debug("QuEst source locale: " + sourceLocale.toString());
		logger.debug("QuEst target locale: " + targetLocale.toString());
		logger.debug("Start of batching successfully completed!");
		return event;
	}

	@Override
	protected Event handleStartDocument (Event event) {
		if (targetInput != null) {
			initializeTargetFilter();
		}
		return event;
	}
	
	@Override
	protected Event handleEndBatch (Event event) {
		//Create temporary source and target language files containing all sentences read.
		String tempSource = params.getInputFolder() + File.separator + "source_data.txt";
		String tempTarget = params.getInputFolder() + File.separator + "target_data.txt";
		BufferedWriter bw = null;
		try {
			logger.debug("sourceSentList size: {}", sourceSentList.size());
			bw = new BufferedWriter(new FileWriter(new File(tempSource)));
			for (String sentence : sourceSentList) {
				bw.write(sentence.trim());
				bw.newLine();
			}
			bw.close();

			logger.debug("targetSentList size: {}", targetSentList.size());
			bw = new BufferedWriter(new FileWriter(new File(tempTarget)));
			for (String sentence : targetSentList) {
				bw.write(sentence.trim());
				bw.newLine();
			}
			bw.close();
			
			questProcessor.run(tempSource, tempTarget);
			
			File deleter = new File(tempSource);
			deleter.delete();
			deleter = new File(tempTarget);
			deleter.delete();
		}
		catch ( IOException e ) {
			logger.error(e.getLocalizedMessage());
		}
		finally {
			if ( bw != null ) {
				try {
					bw.close();
				}
				catch (IOException e) {
					logger.error(e.getLocalizedMessage());
				}
			}
		}

		logger.debug("Batch ending successfully completed!");
		return event;
	}

	@Override
	protected Event handleEndDocument(Event event) {
		return event;
	}

	@Override
	protected Event handleTextUnit(Event sourceEvent) {
		ITextUnit sourceTu = sourceEvent.getTextUnit();
		// Process from two input document
		if ( targetInput != null ) {
			if ( !sourceTu.isTranslatable() || sourceTu.isEmpty() ) {
				return sourceEvent;
			}

			// Align the two entries
			Event targetEvent = synchronize(EventType.TEXT_UNIT, sourceTu);
			ITextUnit targetTu = targetEvent.getTextUnit();

			// Add them to source and target sentence lists.
			sourceSentList.add(sourceTu.toString().trim());
			targetSentList.add(targetTu.toString().trim());
		}
		else {
			// Or process from pre-aligned data (e.g. a TMX file)
			ISegments srcSegs = sourceTu.getSourceSegments();
			if ( !sourceTu.hasTarget(targetLocale) ) {
				return sourceEvent; // Nothing to do
			}
			
			// Get segments of text units and add them to source and target sentence lists.
			ISegments trgSegs = sourceTu.getTargetSegments(targetLocale);
			for (Segment srcSeg : srcSegs) {
				Segment trgSeg = trgSegs.get(srcSeg.getId());
				if ( trgSeg == null ) continue; // Not a pair
				if ( srcSeg.getContent().hasText() && trgSeg.getContent().hasText() ) {
					sourceSentList.add(TextUnitUtil.getText(srcSeg.getContent()));
					targetSentList.add(TextUnitUtil.getText(trgSeg.getContent()));
				}
			}
		}
		return sourceEvent;
	}

	private Event synchronize (EventType untilType,
		ITextUnit sourceTu)
	{
		boolean found = false;
		Event event = null;
		while ( !found && filter.hasNext() ) {
			event = filter.next();
			if ( event.isTextUnit() ) {
				ITextUnit stu = event.getTextUnit();
				// Skip non-translatable and empty just like our primary filter
				if ( !stu.isTranslatable() || stu.isEmpty() ) {
					continue;
				}
			}
			found = (event.getEventType() == untilType);
		}
		if ( !found ) {
			String targetDoc = (targetInput == null) ? "null" : targetInput.getInputURI().toString();
			throw new RuntimeException(
				"Different number of source or target TextUnits. "
					+ "The source and target documents are not paragraph aligned at:\n"
					+ "Source: " + sourceTu.getName() + " <> "
					+ sourceTu.getSource().toString()
					+ "\nTarget Document: " + targetDoc);
		}
		return event;
	}

}
