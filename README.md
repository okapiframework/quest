The aim of the project is to integrate the **QuEst** project -- an open source open source tool for translation quality estimation (http://www.quest.dcs.shef.ac.uk/) -- into the Okapi Framework (http://okapiframework.org) as a step that can be seamlessly with other components.

The project is sponsored by the European Association for Machine Translation (EAMT: http://www.eamt.org/) and led by the NLP group at the University of Sheffield (http://nlp.shef.ac.uk/).

You can try it using [the QuEst Plugin](http://okapiframework.org/wiki/index.php?title=QuEst_Plugin).